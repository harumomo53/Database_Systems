-- P1
SELECT E.empid,
        (SELECT P2.phonenumber
         FROM Phones as P2
         WHERE (E.empid, P2.phonetype) = (P2.empid, 'home')) as homePhoneNumber,
        (SELECT P3.phonenumber
         FROM Phones as P3
         WHERE (E.empid, P3.phonetype) = (P3.empid, 'cell')) as cellPhoneNumber
FROM Employees as E;


-- P2
-- With creates 2 tables 
-- promo_total sums up all of the sales amount for each promo name
-- max_total finds the max number in promo_total so that we can find the highest total 
--      amount for each promo name
-- Then in the main clause, we use max_total to find the sales person with highest sale
-- Union any possibility of no one has selled during the promotion
WITH promo_total(promoName, totalNum) AS
        (SELECT P.promo, sum(S.amount)
        FROM Sales as S, Promotions as P
        WHERE (S.saledate BETWEEN P.startdate AND P.enddate)
        GROUP BY P.promo, S.salesperson),
max_total(promoName, maxNum) AS
        (SELECT P.promoName, max(P.totalNum)
        FROM promo_total AS P
        GROUP BY P.promoName)

(SELECT    P.promo, S.salesperson, sum(S.amount)
FROM      Promotions as P, Sales as S
WHERE     S.saledate BETWEEN P.startdate AND P.enddate
GROUP BY  P.promo, S.salesperson
HAVING    sum(S.amount) >= all (SELECT MX.maxNum
                                FROM max_total AS MX
                                WHERE MX.promoName = P.promo)
ORDER BY  P.promo)
UNION
(SELECT   P.promo, null, null
FROM      Promotions as P, Sales as S
WHERE     (NOT EXISTS
                (SELECT S.saledate
                 FROM Sales as S3
                 WHERE S3.saledate BETWEEN P.startdate AND P.enddate)));
