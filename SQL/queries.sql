-- P1
SELECT pname
FROM   prof, dept
WHERE  numphds < 50;


-- P2
SELECT sname
FROM   student s
WHERE  s.gpa =
       (SELECT min(s2.gpa)
        FROM   student s2);


-- P3
SELECT   data.cno, data.sectno, avg(data.gpa)
FROM     ((enroll join section using (cno, sectno, dname))join student using (sid)) as data
WHERE    data.dname = 'Computer Sciences'
GROUP BY data.cno, data.sectno;


-- P4
SELECT   data.cname, data.sectno
FROM     ((section join enroll using (cno, sectno, dname)) join course using (cno, dname)) as data
GROUP BY data.cname, data.sectno
HAVING   count(data.sid) > 6;


-- P5
SELECT   data.sname, data.sid
FROM     ((student join enroll using (sid)) join section using (cno, sectno, dname)) as data
GROUP BY data.sname, data.sid
HAVING   count(distinct(data.cno, data.sectno, data.dname)) >= all (SELECT count(distinct(data2.cno, data2.sectno, data2.dname))
                                FROM ((student join enroll using (sid)) join section using (cno, sectno, dname)) as data2
                                GROUP BY data2.sid);

-- P6
SELECT   distinct(data.dname)
FROM     ((student join major using (sid)) join dept using (dname)) as data
WHERE    EXISTS (SELECT data2.sid
                 FROM  ((student join major using (sid)) join dept using (dname)) as data2
                 WHERE (data2.age < 18) AND (data.dname = data2.dname));


-- P7
SELECT   student.sname, major.dname
FROM     ((course join enroll using (cno, dname)) join student using (sid)) join major using (sid)
WHERE    course.cname = 'College Geometry 1' OR course.cname = 'College Geometry 2';


-- P8
SELECT dept.dname, dept.numphds
FROM   (course join enroll using (cno, dname)) join major using (sid), dept
WHERE  major.dname = dept.dname AND not exists (SELECT major.sid
                                                FROM   (course join enroll  using (cno, dname)) join major using (sid), dept as dept2
                                                WHERE  major.dname = dept2.dname AND dept.dname = dept2.dname
                                                        AND (course.cname = 'College Geometry 1' OR course.cname = 'College Geometry 2'))
GROUP BY dept.dname, dept.numphds;


-- P9 : avoid nested queries
(SELECT student.sname
FROM course, enroll, student
WHERE (student.sid, course.cno, course.dname) = (enroll.sid, enroll.cno, enroll.dname) AND course.dname = 'Computer Sciences')
INTERSECT
(SELECT student.sname
FROM course, enroll, student
WHERE (student.sid, course.cno, course.dname) = (enroll.sid, enroll.cno, enroll.dname) AND course.dname = 'Mathematics');

-- P10
SELECT (max(student.age) - min(student.age)) AS age_difference
FROM student, major
WHERE (student.sid, major.dname) = (major.sid, 'Computer Sciences');


-- P11
SELECT dept.dname, avg(student.gpa)
FROM   student, dept, major
WHERE  (student.sid, dept.dname) = (major.sid, major.dname) AND 1 > some (SELECT student.gpa
                                                                          FROM   student, dept as dept2, major
                                                                          WHERE  (student.sid, dept2.dname, dept2.dname) = (major.sid, major.dname, dept.dname))
GROUP BY dept.dname;


-- P12
SELECT S.sid, S.sname, S.gpa
FROM   student as S
WHERE  NOT EXISTS((SELECT cno FROM enroll WHERE dname = 'Civil Engineering') EXCEPT (SELECT E.cno FROM enroll as E WHERE E.sid = S.sid));

