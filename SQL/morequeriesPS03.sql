CREATE VIEW ordered_time as ( 
SELECT sitter,baby,start_time as time, 1 as event_count
FROM sessions
GROUP BY sitter,baby, start_time
UNION  
SELECT sitter,baby,end_time as time, -1 as event_count
FROM sessions
GROUP BY sitter,baby, end_time
) ORDER BY sitter,time;


CREATE VIEW count_event_count as
(SELECT sitter, baby, o1.time, event_count, (SELECT sum(event_count)
                               FROM ordered_time as o2                               
                               WHERE ((o2.time < o1.time AND o2.sitter = o1.sitter)
                                        OR (o2.time = o1.time AND o2.event_count = 1 AND o2.sitter = o1.sitter))) as sum_count
FROM ordered_time as o1
GROUP BY sitter, baby, o1.time, event_count
ORDER BY sitter, o1.time, event_count desc
);

CREATE VIEW max_count as
(SELECT sitter, baby, c1.time, event_count, (SELECT max(sum_count)
                               FROM count_event_count as c2
                               WHERE c2.time <= c1.time AND c2.sitter = c1.sitter) as max_count
FROM count_event_count as c1
GROUP BY sitter, baby, c1.time, event_count
ORDER BY sitter, c1.time, event_count desc
);


SELECT baby, max_count
FROM max_count
WHERE event_count = -1;
                          
